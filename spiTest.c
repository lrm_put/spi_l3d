/*
 * spiSpeed.c:
 *	Code to measure the SPI speed/latency.
 *	Copyright (c) 2014 Gordon Henderson
 ***********************************************************************
 * This file is part of wiringPi:
 *	https://projects.drogon.net/raspberry-pi/wiringpi/
 *
 *    wiringPi is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    wiringPi is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with wiringPi.
 *    If not, see <http://www.gnu.org/licenses/>.
 ***********************************************************************
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include "L3G.h"
//#include <fcntl.h>
//#include <sys/ioctl.h>
//#include <linux/spi/spidev.h>

#include <wiringPi.h>
#include <wiringPiSPI.h>

#define	TRUE	(1==1)
#define	FALSE	(!TRUE)

#define	SPI_CHAN		0
#define	NUM_TIMES		100
#define	MAX_SIZE		(1024*1024)

static int myFd ;

int writeDataSPI(uint8_t reg, unsigned char data)
{
unsigned char myData[2];
myData[0] = reg | WRITE_DATA; 
myData[1] = data;
int size = 2;
int spiFail = FALSE;
if (wiringPiSPIDataRW (SPI_CHAN, myData, size) == -1)
        {
          printf ("SPI failure: %s\n", strerror (errno)) ;
          spiFail = TRUE ;
        }
if(spiFail) return 1;

return 0;
}

unsigned char readDataSPI(uint8_t reg)
{
unsigned char myData[2];
myData[0] = reg | READ_DATA; 
myData[1] = 0x00;
int size = 2;
int spiFail = FALSE;
if (wiringPiSPIDataRW (SPI_CHAN, myData, size) == -1)
        {
          printf ("SPI failure: %s\n", strerror (errno)) ;
          spiFail = TRUE ;
        }
if(spiFail) return 1;
printf("Sensor data: %02X, %02X \n", myData[0],myData[1]);
return myData[0];
}


void spiSetup (int speed)
{
  if ((myFd = wiringPiSPISetup (SPI_CHAN, speed)) < 0)
  {
    fprintf (stderr, "Can't open the SPI bus: %s\n", strerror (errno)) ;
    exit (EXIT_FAILURE) ;
  }
}


int main (void)
{

  wiringPiSetup () ;
  spiSetup(1*1000000);
//device initialization
  writeDataSPI(L3G_CTRL_REG1, 0b00001111);
  writeDataSPI(L3G_CTRL_REG4, 0b00110000);
  readDataSPI(L3G_WHO_AM_I);

  close (myFd) ;
  return 0 ;
}
